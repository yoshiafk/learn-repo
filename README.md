## Learn Branch

This is for learning purpose

## Usage

use git bash or terminal to clone

If you using SSH

```bash
git clone git@gitlab.com:yoshiafk/learn-repo.git
```

or if you using htpps

```bash
git clone https://gitlab.com/yoshiafk/learn-repo.git
```

Don't forget to create your own branch

```bash
git branch [branch name]
```

please use

```bash
git switch [branch name]
```

to switch to your own branch, also use

```bash
git push origin [branch name]
```

to push your commit to your branch, and don't forget to make merge request.

lastly, please use

```bash
git pull
```

for pulling data from master to keep updated with the changes before making any changes.

Thank You
